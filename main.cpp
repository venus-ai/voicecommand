#include "home.h"
#include "library/voce.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    voce::init("/home/abrar/Desktop/Projects/voicecommand/lib/", true, true, "/home/abrar/Desktop/Projects/voicecommand/lib/gram/", "digits");
    // voce::init("/home/abrar/Desktop/Projects/voce-0.9.1/lib/", true, false, "", "");
    std::cout << "This is a speech recognition test. "
        << "Speak digits from 0-9 into the microphone. "
        << "Speak 'quit' to quit." << std::endl;
    voce::synthesize("This is a speech synthesis test.");
        voce::synthesize("Type a message to hear it spoken aloud.");

    bool quit = false;
    while (!quit)
    {
        while (voce::getRecognizerQueueSize() > 0)
        {
            std::string s = voce::popRecognizedString();

            // Check if the string contains 'quit'.
            if (std::string::npos != s.rfind("Exit"))
            {
                quit = true;
            }

            std::cout << "You said: " << s << std::endl;
            voce::synthesize(s);
        }
    }

    voce::destroy();
    Home w;
    w.show();

    return a.exec();
}
