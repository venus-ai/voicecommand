#include <QtTextToSpeech>
#include <QDebug>

#include "home.h"
#include "ui_home.h"

Home::Home(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Home)
{
    ui->setupUi(this);

}

Home::~Home()
{
    delete ui;
}

void Home::on_pushButton_clicked()
{

    QTextToSpeech *speech = new QTextToSpeech(this);
    speech->setVolume(1.0);
    speech->setRate(0.0);
    speech->setPitch(1.0);
    speech->say("Hi Shaber");

    qDebug() << "Said";
}
