#-------------------------------------------------
#
# Project created by QtCreator 2019-10-05T10:34:20
#
#-------------------------------------------------

QT       += widgets core gui texttospeech

TARGET = voicecommand
TEMPLATE = app

LIBS += -L/usr/bin/java -L/usr/lib/jvm/java-8-openjdk/jre/lib/amd64/server/ -ljvm

#DEPENDPATH  += /usr/lib/jvm/java-8-openjdk/include/ .
INCLUDEPATH += /usr/lib/jvm/java-8-openjdk/include/

#DEPENDPATH  += /usr/lib/jvm/java-8-openjdk/include/linux/ .
INCLUDEPATH += /usr/lib/jvm/java-8-openjdk/include/linux/

CONFIG += c++11

SOURCES += \
        main.cpp \
        home.cpp

HEADERS += \
        home.h \
        library/voce.h

FORMS += \
        home.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
